# Начало работы над Gradeo

## Зависимости
* .NET 5
* PostgreSQL 11+
* Visual Studio 2019 16.8+

## Структура проекта
* `src` - исходный код проекта
* `docs` - документация и различная полезная информация
* `infrastructure` - всё, что относится к инфраструктуре проекта.

## Управление миграциями
Миграции используются для версионирования структуры базы данных и её соответствия `code-first` структуре моделей наших сущностей.
Миграции также позволяют обновлять не только структуру БД, а также и сами данные, приводя их в соответствие с обновленной структурой.

Подробное руководство по миграциям: https://docs.microsoft.com/en-us/ef/core/managing-schemas/migrations/?tabs=dotnet-core-cli

Пример миграции:
```csharp
using Microsoft.EntityFrameworkCore.Migrations;

namespace Gradeo.Infrastructure.Migrations
{
    public partial class SomeMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {

        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
```

В методе `Up` располагается код, который приводит базу данных в соответствие с измененной схемой сущности.
Метод `Down` предназначен для отмены изменений.

Эти методы создаются и наполняются кодом автоматически, при создании миграции.
Вы можете изменить код этих методов любым удобным образом, в некоторых случаях это даже необходимо.

Для того, чтобы создать миграцию, выполните следующую команду в папке `src/Gradeo.Infrastructure`:

```bash
dotnet ef migrations add <name> --startup-project ..\Gradeo.Api\
```

Для того, чтобы применить миграции, выполните следующую команду в папке `src/Gradeo.Infrastructure`:

```bash
dotnet ef database update --startup-project ..\Gradeo.Api\
```

Если вдруг миграция не подходит(некорректный код и т.п.), то её всегда можно отменить следующим образом:

```bash
dotnet ef migrations remove --startup-project ..\Gradeo.Api\
```