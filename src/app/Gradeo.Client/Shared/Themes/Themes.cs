﻿using MudBlazor;

public class Themes
{
    public static MudTheme LightTheme => new()
    {
        Palette = new Palette()
        {
            Primary = "#7bc96f",
            Secondary = Colors.Shades.White,
            AppbarText = "#383838",
            PrimaryContrastText = Colors.Shades.White,
            SecondaryContrastText = "#383838",
            Background = "#f1f2f3",
        },
    };
}
