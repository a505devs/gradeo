﻿namespace Gradeo.Client.Shared.Utils
{
    public interface IAccessTokenService
    {
        string? AccessToken { get; set; }
        void RemoveToken();
    }
}
