﻿using System;

namespace Gradeo.Client.Shared.Utils
{
    public class AccessTokenService : IAccessTokenService
    {
        private string? _accessToken;

        public string? AccessToken
        {
            get => _accessToken;
            set
            {
                if (!string.IsNullOrWhiteSpace(value))
                {
                    _accessToken = value;
                }
            }
        }

        public void RemoveToken() => _accessToken = null;
    }
}
