﻿using System;
using System.Threading.Tasks;
using Blazored.LocalStorage;

namespace Gradeo.Client.Shared.Utils
{
    public class DeviceIdService
    {
        private readonly ILocalStorageService _localStorage;

        public DeviceIdService(ILocalStorageService localStorage)
        {
            _localStorage = localStorage;
        }

        public async Task<string> GetDeviceId()
        {
            if (await _localStorage.ContainKeyAsync("deviceId"))
            {
                return await _localStorage.GetItemAsync<string>("deviceId");
            }

            string deviceId = Guid.NewGuid().ToString();
            await _localStorage.SetItemAsync("deviceId", deviceId);
            return deviceId;
        }
    }
}
