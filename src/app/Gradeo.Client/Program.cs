using System;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Components.WebAssembly.Hosting;
using Microsoft.Extensions.DependencyInjection;
using MudBlazor;
using MudBlazor.Services;
using Blazored.LocalStorage;
using Gradeo.Client.Authentication;
using Gradeo.Client.Shared.Utils;
using Microsoft.AspNetCore.Components.Authorization;
using Toolbelt.Blazor.Extensions.DependencyInjection;

namespace Gradeo.Client
{
    public class Program
    {
        public static async Task Main(string[] args)
        {
            var builder = WebAssemblyHostBuilder.CreateDefault(args);
            builder.RootComponents.Add<App>("#app");

            builder.Services.AddScoped(sp => new HttpClient
            {
                BaseAddress = new Uri(builder.Configuration["ApiUrl"] ?? builder.HostEnvironment.BaseAddress),
            });

            // Authorization
            builder.Services.AddSingleton<IAccessTokenService, AccessTokenService>();
            builder.Services.AddScoped<AuthenticationStateProvider, AuthenticationProvider>();
            builder.Services.AddScoped<IAuthenticationService, AuthenticationService>();
            builder.Services.AddScoped<DeviceIdService>();

            builder.Services.AddOptions();
            builder.Services.AddAuthorizationCore();

            builder.Services
                .AddMudServices()
                .AddBlazoredLocalStorage()
                .AddHeadElementHelper();

            await builder.Build().RunAsync();
        }
    }
}
