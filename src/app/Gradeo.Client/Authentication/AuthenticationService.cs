﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Http.Json;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using Gradeo.Client.Shared.Utils;
using Gradeo.Shared;
using Gradeo.Shared.Authentication;
using Microsoft.AspNetCore.Components.Authorization;

namespace Gradeo.Client.Authentication
{
    public class AuthenticationService: IAuthenticationService
    {
        private readonly HttpClient _client;
        private readonly AuthenticationStateProvider _authStateProvider;
        private readonly IAccessTokenService _accessTokenService;

        public AuthenticationService(
            HttpClient client,
            AuthenticationStateProvider authStateProvider,
            IAccessTokenService accessTokenService
            )
        {
            _client = client;
            _authStateProvider = authStateProvider;
            _accessTokenService = accessTokenService;
        }

        public Task<ApiResponse> RegisterUser(RegisterDto registerDto) => throw new System.NotImplementedException();

        public async Task<LoginResultDto> Login(LoginDto loginDto)
        {
            try
            {
                var response = await _client.PostAsJsonAsync
                    ("/api/authentication/login", loginDto);
                var result = await response.Content.ReadFromJsonAsync<LoginResultDto>();
                _accessTokenService.AccessToken = result?.AccessToken;
                ((AuthenticationProvider)_authStateProvider).NotifyUserAuthentication(loginDto.Username);
                _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("bearer",
                    result?.AccessToken ?? string.Empty);
                return result!;
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex);
                throw;
            }

        }

        public Task Logout()
        {
            _accessTokenService.RemoveToken();
            ((AuthenticationProvider)_authStateProvider).NotifyUserLogout();
            _client.DefaultRequestHeaders.Authorization = null;
            return Task.CompletedTask;
        }
    }
}
