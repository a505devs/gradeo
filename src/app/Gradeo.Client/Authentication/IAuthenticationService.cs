﻿using System.Threading.Tasks;
using Gradeo.Shared;
using Gradeo.Shared.Authentication;

namespace Gradeo.Client.Authentication
{
    public interface IAuthenticationService
    {
        Task<ApiResponse> RegisterUser(RegisterDto registerDto);
        Task<LoginResultDto> Login(LoginDto loginDto);
        Task Logout();
    }
}
