﻿using System;

namespace Gradeo.Shared.Institutes
{
    public class InstituteTableItemDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; } = default!;
        public string ShortName { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}
