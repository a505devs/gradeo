﻿using System.ComponentModel.DataAnnotations;

namespace Gradeo.Shared.Institutes
{
    public class InstituteCreateDto
    {
        [Required]
        [MinLength(2)]
        public string Name { get; set; }

        public string? ShortName { get; set; }
    }
}
