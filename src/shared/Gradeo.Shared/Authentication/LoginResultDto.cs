﻿using System;

namespace Gradeo.Shared.Authentication
{
    public class LoginResultDto
    {
        public string AccessToken { get; set; } = string.Empty;
        public DateTime Expiration { get; set; }
    }
}
