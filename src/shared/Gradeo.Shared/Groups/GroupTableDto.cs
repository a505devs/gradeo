﻿using System;

namespace Gradeo.Shared.Groups
{
    public class GroupTableDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; } = default!;
        public string DepartmentName { get; set; } = default!;
        public string Profile { get; set; } = default!;

    }
}
