﻿using System;
using Gradeo.Core.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace Gradeo.Infrastructure
{
    public class GradeoDbContext: IdentityDbContext<ApplicationUser, ApplicationRole, Guid>
    {
        public DbSet<Institute> Institutes => Set<Institute>();
        
        public GradeoDbContext(DbContextOptions<GradeoDbContext> options) : base(options)
        {
        }
    }
}
