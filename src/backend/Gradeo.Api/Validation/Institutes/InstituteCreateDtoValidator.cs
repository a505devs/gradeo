﻿using System.Linq;
using FluentValidation;
using Gradeo.Infrastructure;
using Gradeo.Shared.Institutes;
using Microsoft.EntityFrameworkCore;

namespace Gradeo.Api.Validation.Institutes
{
    public class InstituteCreateDtoValidator: AbstractValidator<InstituteCreateDto>
    {
        public InstituteCreateDtoValidator(GradeoDbContext context)
        {
            RuleFor(m => m.Name)
                .Cascade(CascadeMode.Stop)
                .NotEmpty()
                .WithMessage("Имя должно быть задано")
                .MustAsync(async (value, token) =>
                {
                    bool alreadyExists = await context.Institutes.AnyAsync(u => u.Name == value, token);
                    return !alreadyExists;
                })
                .WithMessage("Имя института должно быть уникальным");

        }
    }
}
