﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Gradeo.Core.Dtos.PostDocSpeciality;
using Gradeo.Core.Entities;
using Gradeo.Core.Exceptions;
using Gradeo.Infrastructure;
using Gradeo.Shared.Groups;
using Gradeo.Shared.Institutes;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace Gradeo.Api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class InstitutesController : ControllerBase
    {
        private readonly GradeoDbContext _context;

        public InstitutesController(GradeoDbContext context)
        {
            _context = context;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<InstituteTableItemDto>>> GetInstitutes()
        {
            return Ok(await _context.Institutes
                .Select(institute =>
                    new InstituteTableItemDto
                    {
                        Id = institute.Id,
                        Name = institute.Name,
                        ShortName = institute.ShortName,
                        CreatedAt = institute.CreatedDate
                    })
                .ToListAsync()
            );
        }

        [HttpPost]
        public async Task<IActionResult> CreateInstitute([FromBody] InstituteCreateDto dto)
        {
            var existingInstitute = _context.Institutes
                .SingleOrDefault(u => u.Name == dto.Name);

            if (existingInstitute is not null)
            {
                throw new DuplicateEntityException(dto.Name);
            }

            Institute institute = new() {Name = dto.Name, ShortName = dto.ShortName};

            await _context.Institutes.AddAsync(institute);
            await _context.SaveChangesAsync();

            return Created($"/api/institutes/{institute.Id}", null);
        }
    }
}
