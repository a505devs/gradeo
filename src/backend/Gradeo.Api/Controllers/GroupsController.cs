﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Gradeo.Core.Dtos.PostDocSpeciality;
using Gradeo.Core.Exceptions;
using Gradeo.Shared.Groups;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Gradeo.Api.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class GroupsController : ControllerBase
    {
        [HttpGet]
        public async Task<IEnumerable<GroupTableDto>> Get()
        {
            return new List<GroupTableDto>
            {
                new()
                {
                    Id = Guid.NewGuid(),
                    Name = "ИВТ-20-1о",
                    DepartmentName = "ИТиКС",
                    Profile = "Информатика и вычислительная техника"
                },
                new()
                {
                    Id = Guid.NewGuid(),
                    Name = "ИВТ-20-2о",
                    DepartmentName = "ИТиКС",
                    Profile = "Информатика и вычислительная техника"
                },
                new()
                {
                    Id = Guid.NewGuid(),
                    Name = "ПИН-20-1о",
                    DepartmentName = "ИТиКС",
                    Profile = "Программная инженерия"
                },
            };
        }
    }
}
