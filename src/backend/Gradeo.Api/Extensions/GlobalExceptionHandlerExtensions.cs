﻿using System.Text.Json;
using System.Threading.Tasks;
using Gradeo.Core.Exceptions;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Http;
using static Microsoft.AspNetCore.Http.StatusCodes;

namespace Gradeo.Api.Extensions
{
    public static class GlobalExceptionHandlerExtensions
    {
        private const string DefaultExceptionMessage = "An error occurred.";

        private static (int status, string message) GetErrorResponse(DomainException exception)
        {
            return exception switch
            {
                EntityNotFoundException => (Status404NotFound, "Entity not found"),
                _ => (Status500InternalServerError, DefaultExceptionMessage)
            };
        }

        private static async Task WriteErrorResponse(this HttpContext context, int status, string message)
        {
            context.Response.StatusCode = status;
            await context.Response.WriteAsync(JsonSerializer.Serialize(new
            {
                StatusCode = status,
                Message = message
            }));
        }

        public static IApplicationBuilder UseGlobalExceptionHandler(this IApplicationBuilder app)
        {
            app.UseExceptionHandler(errorApp =>
            {
                errorApp.Run(async context =>
                {
                    context.Response.ContentType = "application/json";

                    var exceptionHandlerPathFeature =
                        context.Features.Get<IExceptionHandlerPathFeature>();

                    if (exceptionHandlerPathFeature.Error is DomainException domainException)
                    {
                        (int status, string message) = GetErrorResponse(domainException);
                        await context.WriteErrorResponse(status, message);
                    }
                    else
                    {
                        await context.WriteErrorResponse(
                            Status500InternalServerError, DefaultExceptionMessage);
                    }
                });
            });

            return app;
        }
    }
}
