﻿using System;
using Microsoft.AspNetCore.Identity;

namespace Gradeo.Core.Entities
{
    public class ApplicationRole: IdentityRole<Guid>
    {
        public ApplicationRole(string name): base(name) {}
    }
}
