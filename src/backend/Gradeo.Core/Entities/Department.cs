﻿using System;

namespace Gradeo.Core.Entities
{
    public class Department: BaseEntity
    {
        public string Name { get; set; }
        public string? ShortName { get; set; }

        public Guid InstituteId { get; set; }
        public Institute Institute { get; set; }
    }
}
