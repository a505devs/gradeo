﻿using System;
using Gradeo.Core.Abstractions;

namespace Gradeo.Core.Entities
{
    public class Group : BaseEntity, IHasName
    {
        public Speciality Speciality { get; set; }
        public Guid SpecialityId { get; set; }

        public Guid? ProfileId { get; set; }
        public Profile? Profile { get; set; }

        public EducationForm EducationForm { get; set; } = EducationForm.FullTime;

        public Guid DepartmentId { get; set; }
        public Department Department { get; set; }

        public string Name { get; set; }
    }

    public enum EducationForm
    {
        FullTime,
        PartTime
    }
}
