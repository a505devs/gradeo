﻿using Gradeo.Core.Abstractions;

namespace Gradeo.Core.Entities
{
    public class Institute: BaseEntity, IHasName
    {
        public string Name { get; set; }
        public string? ShortName { get; set; }
    }
}
