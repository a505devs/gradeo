﻿using System;
using Microsoft.AspNetCore.Identity;

namespace Gradeo.Core.Entities
{
    public class ApplicationUser : IdentityUser<Guid>
    {

    }
}
