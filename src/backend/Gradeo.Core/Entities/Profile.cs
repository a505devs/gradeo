﻿using System;
using Gradeo.Core.Abstractions;

namespace Gradeo.Core.Entities
{
    public class Profile : BaseEntity, IHasName
    {
        public string Name { get; set; }
        public string? Code { get; set; }

        public Guid SpecialityId { get; set; }
        public Speciality Speciality { get; set; }
    }
}
