﻿namespace Gradeo.Core.Entities
{
    public enum SpecialityLevel
    {
        Bachelor,
        Master,
        Graduate,
    }
}
