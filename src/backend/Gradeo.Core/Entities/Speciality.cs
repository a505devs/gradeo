﻿using Gradeo.Core.Abstractions;

namespace Gradeo.Core.Entities
{
    public class Speciality: BaseEntity, IHasName
    {
        public string Name { get; set; }
        public string Code { get; set; }

        public SpecialityLevel Level { get; set; } = SpecialityLevel.Bachelor;
    }
}
