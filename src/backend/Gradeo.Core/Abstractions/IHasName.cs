﻿namespace Gradeo.Core.Abstractions
{
    public interface IHasName
    {
        string Name { get; }
    }
}
