﻿namespace Gradeo.Core.Exceptions
{
    public class DuplicateEntityException : DomainException
    {
        public DuplicateEntityException(string name)
            : base($"Entity with name {name} already exists")
        {
        }
    }
}
