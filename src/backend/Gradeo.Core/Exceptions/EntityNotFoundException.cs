﻿namespace Gradeo.Core.Exceptions
{
    public class EntityNotFoundException: DomainException
    {
        public EntityNotFoundException(string name)
            : base($"Entity with name {name} not found")
        {
        }
    }
}
