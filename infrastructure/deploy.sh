#!/usr/bin/env bash

# Копируем docker-compose.yml (на случай, если он вдруг изменится)
echo "Updating docker-compose.yml..."
scp -i gradeo docker-compose.yml gradeo@$SERVER_URL:gradeo/docker-compose.yml

# Останавливаем и обновляем все сервисы
echo "Updating services..."
ssh -i gradeo -t gradeo@$SERVER_URL "cd gradeo && docker-compose stop && docker-compose pull && docker-compose up -d"
